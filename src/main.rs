use rand::seq::SliceRandom;
use std::fs;
use std::io;
use std::io::prelude::*;
extern crate lapp;

fn main() {
    let stdin = io::stdin();

    let args = lapp::parse_args("\n\nHangman Game
    -l, --list (path default wordlist) Path to a newline seperated list of words.
    -s, --size (integer default 5) Minimum word size.");

    let path = args.get_path("list");
    let size = args.get_integer("size") as usize;

    let file = fs::File::open(path).expect("File open fail.");
    let reader = io::BufReader::new(file);
    let lines: Vec<String> = reader
        .lines()
        .map(|line| line.expect("Line parse fail."))
        .filter(|line| line.len() >= size)
        .collect();

    let mut rng = rand::thread_rng();

    loop {
        let word = lines.choose(&mut rng).unwrap();

        let mut chances: u8 = 5;
        let mut wrong = "".to_owned();
        let mut found = "_".repeat(word.len());

        print!(
            "solved {} | chances {} | wrong '{:5}'  ",
            found, chances, wrong
        );
        io::stdout().flush().unwrap();

        for line in stdin.lock().lines() {
            for line_char in line.unwrap().chars() {
                if word.find(line_char) == None {
                    if wrong.find(line_char) != None {
                        continue;
                    }
                    wrong.push(line_char);
                    chances = chances - 1;
                } else {
                    for word_char in word.chars().enumerate() {
                        if line_char == word_char.1 {
                            found.replace_range(
                                word_char.0..word_char.0 + 1,
                                &line_char.to_string(),
                            );
                        }
                    }
                }
                if chances == 0 {
                    break;
                }
            }

            print!(
                "solved {} | chances {} | wrong '{:5}'  ",
                found, chances, wrong
            );
            io::stdout().flush().unwrap();

            if chances == 0 || found.find('_') == None {
                break;
            }
        }

        if chances > 0 {
            if found.find('_') != None {
                break;
            }
            println!("\nVICTORY!\n");
        } else {
            println!("\n       {}\nDEFEAT!\n", word);
        }
    }
}
